use info;

INSERT INTO info (id, full_name, trust_level) VALUES (1, 'Nick Smith', 0.50);
INSERT INTO info (id, full_name, trust_level) VALUES (2, 'Nina Grande', 0.50);
INSERT INTO info (id, full_name, trust_level) VALUES (3, 'Mick Grande', 0.50);
INSERT INTO info (id, full_name, trust_level) VALUES (4, 'Nazar Grande', 0.70);
INSERT INTO info (id, full_name, trust_level) VALUES (5, 'Smith Grande', 0.90);
INSERT INTO info (id, full_name, trust_level) VALUES (6, 'Sara Grande', 0.10);
INSERT INTO info (id, full_name, trust_level) VALUES (7, 'Brain Grande', 0.10);